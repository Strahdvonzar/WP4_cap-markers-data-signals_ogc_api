import os;
import datetime;
import sys;
import aiohttp;
import asyncio;
import psycopg2;
from psycopg2 import sql;
from psycopg2.extras import RealDictCursor
import csv;
import json as JSON;
from classes.amTools import amTool;

class amUser:
 def __init__( self ):
  """Class for User Access Levels"""
  self.amTool = amTool();
  self.amTool.log( "UserAccess Initiated" );
  self.cfg      = self.amTool.load( "./cfg/" , "config" );
  self.isAdmin  = False;
  self.access   = dict();

 async def getUserAccess( self , credentials ):
     async with aiohttp.ClientSession() as session:

          endPoint = ( self.cfg["auth"]["endpoint"] + "?" + 
                       "app_id="      + self.cfg["auth"]["appid"] + 
                       "&app_secret=" + self.cfg["auth"]["secret"] + 
                       "&usr_un="     + credentials["un"] + 
                       "&usr_pw="     + credentials["pw"] 
                     );

          async with session.get( endPoint ) as response:
               myText = await response.text();
               self.amTool.log_curl( "get" , "" , endPoint , False , True );

               try:
                   myJSON = JSON.loads( myText );
                   
                   if isinstance( myJSON , bool ):
                       return False;
                   else:
                       if "error" in myJSON:
                           return False;
                       else:
                           self.isAdmin = myJSON["isadmin"];
                           self.access  = myJSON["access"];
                           return myJSON;
               except ValueError as error :
                   return False;
               return True;

 def hasAccessToParcel( self , parcel ):
     
     hasAccess = False;
     if( self.isAdmin > 0 ):
         return True;

     for accessDetail in self.access :
         if( accessDetail["parcel_id"] == parcel ):
             hasAccess = True;
         else:
             hasAccess = False;

     return hasAccess;