import os;
import datetime;
import sys;
import aiohttp;
import asyncio;
import psycopg2;
from psycopg2 import sql;
from psycopg2.extras import RealDictCursor
import csv;
import json as JSON;
from classes.amTools import amTool;

class amModel:
 def __init__( self ):
  """Class for Queryinjg Database"""
  self.amTool = amTool();
  self.amTool.log( "Model Initiated" );
  self.cfg = self.amTool.load( "./cfg/" , "config" );

 async def createView( self , layer_name ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor            = connection.cursor();
       myLayerTbl        = layer_name;
       mySchema          = self.cfg["database"]["schema"];
       myViewName        = "view_"+layer_name;
       myMetadataTbl     = self.cfg["database"]["tables"]["metadata"];

       create_view_query = ("CREATE OR REPLACE VIEW {mySchema_param}.{myViewName_param} " + 
                              "AS select " + 
                                  "t1.\"fid\", " + 
                                  "t1.\"the_geom\", " + 
                                  "t1.\"Identifier\", " +                       
                                  # "t2.\"parcelcode\" , " + 
                                  "t2.\"calendar\" , " + 
                                  "t2.\"aggregates\" , " + 
                                  "t2.\"computed\" , " + 
                                  "t2.\"image\" " + 
                                  # "t2.\"calculationdate\" , " + 
                                  # "t2.\"rulename\" , " + 
                                  # "t2.\"colorcode\" , " + 
                                  # "t2.\"cultivationdeclaredcode\" , " + 
                                  # "t2.\"cultivationdeclaredname\" , " + 
                                  # "t2.\"cultivation1stpredictioncode\" , " + 
                                  # "t2.\"probability1stprediction\" , " + 
                                  # "t2.\"cultivation2ndpredictioncode\" , " + 
                                  # "t2.\"probability2stprediction\" " + 
                                 "FROM {mySchema_param}.{myLayerTbl_param} t1 " + 
                                   "LEFT JOIN {mySchema_param}.{myMetadataTbl_param} t2 ON t2.layer_id = t1.fid and t2.layer_name = {myLayerName_param};");

       print( "Creating View with name " + str( layer_name ) );
       cursor.execute(
           sql.SQL( create_view_query )
              .format( 
                       mySchema_param      = sql.Identifier( mySchema ) , 
                       myViewName_param    = sql.Identifier( myViewName ) , 
                       myLayerTbl_param    = sql.Identifier( myLayerTbl ) , 
                       myMetadataTbl_param = sql.Identifier( myMetadataTbl ) ,
                       myLayerName_param   = sql.Literal( layer_name ) 
              ), [] 
       );
       cursor.close();
       connection.commit();

    except (Exception, psycopg2.Error) as error :
        print ("Error creating layer view", error);

    finally:
        if(connection):
            connection.close();
            print("[createView] : DB Connection Closed");

 async def checkIfConfigurationExists( self , table_schema , table_name ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor            = connection.cursor();
       mySchema          = self.cfg["database"]["schema"];
       myPKTbl           = self.cfg["database"]["tables"]["foreignkey"];
       myQuery           = ("select table_name from {mySchema_param}.{myPKTbl_param} " + 
                            "where table_name = {myTblName_param} " + 
                            "and table_schema = {myTblSchema_param} ");

       print( "Checking if Foreign Key Configuration already exists for layer " + str( table_name ) );
       cursor.execute(
           sql.SQL( myQuery )
              .format( 
                       mySchema_param      = sql.Identifier( mySchema ) , 
                       myPKTbl_param       = sql.Identifier( myPKTbl ) , 
                       myTblName_param     = sql.Literal( table_name ) , 
                       myTblSchema_param   = sql.Literal( table_schema ) 
              ), [] 
       );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

    except( Exception, psycopg2.Error ) as error :
        print ( "Error Inserting GeoServer Foreign Key Configuration", error );

    finally:
        if( connection ):
            connection.close();
            print( "[checkIfConfigurationExists] : DB Connection Closed" );
            return str( len( my_query_response ) );

 async def addPKConfiguration( self , table_schema , table_name , pk_column , pk_sequence ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       hasEntry = await self.checkIfConfigurationExists( table_schema , table_name );
       if( int( hasEntry ) <= 0 ):
           cursor   = connection.cursor();
           mySchema = self.cfg["database"]["schema"];
           myPKTbl  = self.cfg["database"]["tables"]["foreignkey"];
           myQuery  = ("insert into {mySchema_param}.{myPKTbl_param} " + 
                       " ( table_schema , table_name , pk_column , pk_sequence ) " + 
                       " VALUES " + 
                       " ( {myTblSchema_param} , {myTblName_param} , {myPKClmn_param} , {myPKSeq_param});");
           print( "Inserting GeoServer Foreign Key Configuration for Layer " + str( table_name ) );

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param      = sql.Identifier( mySchema ) , 
                           myPKTbl_param       = sql.Identifier( myPKTbl ) , 
                           myTblName_param     = sql.Literal( table_name ) , 
                           myTblSchema_param   = sql.Literal( table_schema ) , 
                           myPKClmn_param      = sql.Literal( pk_column ) , 
                           myPKSeq_param       = sql.Literal( pk_sequence ) 
                  ), [] 
           );
           cursor.close();
           connection.commit();
       else:
           print( "Entry in Foreign Key Configuration already exists." );

    except( Exception, psycopg2.Error ) as error :
        print ( "Error Inserting GeoServer Foreign Key Configuration", error );

    finally:
        if( connection ):
            connection.close();
            print( "[addPKConfiguration] : DB Connection Closed" );

 async def addMetadata_old( self , options ):
    connection       = False;
    entity           = options["entity"];
    layerName        = options["layername"];
    featureid        = options["featureid"];
    myJSON           = options["json"];
    isUpdatable      = options["isUpdatable"];
    myTable          = self.cfg["database"]["tables"]["metadata"];
    
    try: 
        connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                       password = self.cfg["database"]["connection"]["password"],
                                       host     = self.cfg["database"]["connection"]["host"],
                                       port     = self.cfg["database"]["connection"]["port"],
                                       database = self.cfg["database"]["connection"]["database"] );

        if( isUpdatable is True ) : 
            print( "Updating " + entity );
            cursor   = connection.cursor();
            mySchema = self.cfg["database"]["schema"];

            myQuery  = ("UPDATE "+str( mySchema )+"."+str( myTable )+" " + 
                        " set computed = '"+JSON.dumps( myJSON )+"' " + 
                        " where layer_id = "+str( featureid )+" " + 
                        "and layer_name = '"+str( layerName )+"' ;");

            cursor.execute( myQuery );

            print( "Updating Computed for Layer " + str( layerName ) );

            cursor.close();
            connection.commit();

            return { "success" : True };

        else : 
            print( entity + " exist. Update Prohibited" );
            return { "error" : entity + " exist. Update Prohibited" };
    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };
        print ( "Error Inserting Computed", error );

    finally:
        if( connection ):
            connection.close();
            print( "[addMetadata] : DB Connection Closed" );

 async def addMetadata( self , options ):
    connection       = False;
    entity           = options["entity"];
    layerName        = options["layername"];
    featureid        = options["featureid"];
    myJSON           = options["json"];
    isUpdatable      = options["isUpdatable"];
    myTable          = self.cfg["database"]["tables"]["metadata"];
    hasMetadata      = False;

  # Get current Metadata Object
    response = await self.getMetaData( {
      "featureid"   : featureid , 
      "layer_name"  : layerName 
    } );
    
    if( len( response ) > 0 ):
        if( len( response[0]["computed"] ) > 0 ):
            hasMetadata = True;

    try: 
        connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                       password = self.cfg["database"]["connection"]["password"],
                                       host     = self.cfg["database"]["connection"]["host"],
                                       port     = self.cfg["database"]["connection"]["port"],
                                       database = self.cfg["database"]["connection"]["database"] );
        mySchema = self.cfg["database"]["schema"];

        if( hasMetadata ) :
            if( isUpdatable is True ) : 
                print( "Updating " + entity );
                cursor   = connection.cursor();

                for entryTimestamp in myJSON:
                    response[0]["computed"][entryTimestamp] = myJSON[entryTimestamp];

                myQuery  = ("UPDATE "+str( mySchema )+"."+str( myTable )+" " + 
                            " set computed = '"+JSON.dumps( response[0]["computed"] )+"' " + 
                            " where layer_id = "+str( featureid )+" " + 
                            "and layer_name = '"+str( layerName )+"' ;");

                cursor.execute( myQuery );

                print( "Updating Computed for Layer " + str( layerName ) );

                cursor.close();
                connection.commit();
                return { "success" : True };

            else : 
                print( entity + " exist. Update Prohibited" );
                return { "error" : entity + " exist. Update Prohibited" };
        else:
            cursor   = connection.cursor();
            myQuery  = ("UPDATE "+str( mySchema )+"."+str( myTable )+" " + 
                        " set computed = '"+JSON.dumps( myJSON )+"' " + 
                        " where layer_id = "+str( featureid )+" " + 
                        "and layer_name = '"+str( layerName )+"' ;");

            cursor.execute( myQuery );
            cursor.close();
            connection.commit();
            return { "success" : True };

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };
        print ( "Error Inserting Computed", error );
    finally:
        if( connection ):
            connection.close();
            print( "[addMetadata] : DB Connection Closed" );

 async def addCalendar( self , options ):
    connection       = False;
    entity           = options["entity"];
    layerName        = options["layername"];
    featureid        = options["featureid"];
    myJSON           = options["json"];
    isUpdatable      = options["isUpdatable"];
    myTable          = self.cfg["database"]["tables"]["metadata"];
    
    try: 
        connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                       password = self.cfg["database"]["connection"]["password"],
                                       host     = self.cfg["database"]["connection"]["host"],
                                       port     = self.cfg["database"]["connection"]["port"],
                                       database = self.cfg["database"]["connection"]["database"] );

        if( isUpdatable is True ) : 
            print( "Updating " + entity );
            cursor   = connection.cursor();
            mySchema = self.cfg["database"]["schema"];

            myQuery  = ("UPDATE "+str( mySchema )+"."+str( myTable )+" " + 
                        " set calendar = '"+JSON.dumps( myJSON )+"' " + 
                        " where layer_id = "+str( featureid )+" " + 
                        "and layer_name = '"+str( layerName )+"' ;");

            cursor.execute( myQuery );

            print( "Updating Calendar for Layer " + str( layerName ) );

            cursor.close();
            connection.commit();

            return { "success" : True };

        else : 
            print( entity + " exist. Update Prohibited" );
            return { "error" : entity + " exist. Update Prohibited" };
    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };
        print ( "Error Inserting Calendar", error );

    finally:
        if( connection ):
            connection.close();
            print( "[addCalendar] : DB Connection Closed" );

 async def addAggregates( self , options ):
    connection       = False;
    entity           = options["entity"];
    layerName        = options["layername"];
    featureid        = options["featureid"];
    myJSON           = options["json"];
    isUpdatable      = options["isUpdatable"];
    myTable          = self.cfg["database"]["tables"]["metadata"];

    try: 
        connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                       password = self.cfg["database"]["connection"]["password"],
                                       host     = self.cfg["database"]["connection"]["host"],
                                       port     = self.cfg["database"]["connection"]["port"],
                                       database = self.cfg["database"]["connection"]["database"] );

        if( isUpdatable is True ) : 
            print( "Updating " + entity );
            cursor   = connection.cursor();
            mySchema = self.cfg["database"]["schema"];

            myQuery  = ("UPDATE "+str( mySchema )+"."+str( myTable )+" " + 
                        " set aggregates = '"+JSON.dumps( myJSON )+"' " + 
                        " where layer_id = "+str( featureid )+" " + 
                        "and layer_name = '"+str( layerName )+"' ;");

            cursor.execute( myQuery );

            print( "Updating Aggregates for Layer " + str( layerName ) );

            cursor.close();
            connection.commit();

            return { "success" : True };

        else : 
            print( entity + " exist. Update Prohibited" );
            return { "error" : entity + " exist. Update Prohibited" };
    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };
        print ( "Error Inserting Aggregates", error );

    finally:
        if( connection ):
            connection.close();
            print( "[addAggregates] : DB Connection Closed" );

 async def deleteEntity( self , options ):
    connection = False;

    try: 
       entity           = options["entity"];
       layerName        = options["layername"];
       item             = options["item"] if ( "item" in options ) else False;
       featureid        = options["featureid"] if ( "featureid" in options ) else "";
       queryReturn      = options["return"] if ( "return" in options ) else "*";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );
       cursor     = connection.cursor();

       if ( featureid == "" ):
           myQuery    = ( "DELETE from {mySchema_param}.{myTable_param} " + 
                          "where layer_name = {layerName_param} " + 
                          " returning "+queryReturn+";"
                        );

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                           myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                           layerName_param   = sql.Literal( layerName ) 
                  ), [] 
           );
       else:
           if( item is not False ):
               myQuery    = ( "DELETE from {mySchema_param}.{myTable_param} " + 
                              "where layer_name = {layerName_param} " + 
                              " and layer_id = {featureId_param} " + 
                              " and {unique_key_param} = {item_id_param} " +
                              " returning "+queryReturn+";"
                            );

               cursor.execute(
                   sql.SQL( myQuery )
                      .format( 
                               mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                               myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                               featureId_param   = sql.Literal( featureid ) , 
                               layerName_param   = sql.Literal( layerName ) , 
                               unique_key_param  = sql.Identifier( item["unique_key"] ) , 
                               item_id_param     = sql.Literal( item["id"] ) 
                      ), [] 
               );

           else:
               myQuery    = ( "DELETE from {mySchema_param}.{myTable_param} " + 
                              "where layer_name = {layerName_param} " + 
                              " and layer_id = {featureId_param} " + 
                              " returning "+queryReturn+";"
                            );

               cursor.execute(
                   sql.SQL( myQuery )
                      .format( 
                               mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                               myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                               featureId_param   = sql.Literal( featureid ) , 
                               layerName_param   = sql.Literal( layerName ) 
                      ), [] 
               );

       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if( cursor.rowcount <= 0 ):
           print( "No entries for layer " + str( layerName ) + " in " + str( entity ) );
           return my_query_response;
       else:
           print( str( cursor.rowcount ) + " entries have been removed for layer " + str( layerName ) + " in " + str( entity ) );
           return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        print( error );
        print( "cursor.query : " + str ( cursor.query ) );
        print( "cursor.rowcount : " + str( cursor.rowcount ) );
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[deleteEntity] : DB Connection Closed" );

 async def deleteCalendar( self , options ):
    connection = False;

    try: 
       entity           = options["entity"];
       layerName        = options["layername"];
       featureid        = options["featureid"] if ( "featureid" in options ) else "";
       queryReturn      = options["return"] if ( "return" in options ) else "*";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );
       cursor     = connection.cursor();
       
       if( featureid == "" ):
           myQuery    = ( "UPDATE {mySchema_param}.{myTable_param} set calendar = NULL " + 
                          " where layer_name = {layerName_param} " );

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                           myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                           layerName_param   = sql.Literal( layerName ) 
                  ), [] 
           );
       else:
           myQuery    = ( "UPDATE {mySchema_param}.{myTable_param} set calendar = NULL " + 
                          " where layer_name = {layerName_param} " + 
                          " and layer_id = {featureId_param} ");

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                           myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                           featureId_param   = sql.Literal( featureid ) , 
                           layerName_param   = sql.Literal( layerName ) 
                  ), [] 
           );

       cursor.close();
       connection.commit();

       print( "Calendar has been removed from " + str( layerName ) );
       return { "success" : True };

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[deleteCalendar] : DB Connection Closed" );

 async def deleteAggregates( self , options ):
    connection = False;

    try: 
       entity           = options["entity"];
       layerName        = options["layername"];
       featureid        = options["featureid"] if ( "featureid" in options ) else "";
       queryReturn      = options["return"] if ( "return" in options ) else "*";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );
       cursor     = connection.cursor();
       
       if( featureid == "" ):
           myQuery    = ( "UPDATE {mySchema_param}.{myTable_param} set aggregates = NULL " + 
                          " where layer_name = {layerName_param} " );

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                           myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                           layerName_param   = sql.Literal( layerName ) 
                  ), [] 
           );
       else:
           myQuery    = ( "UPDATE {mySchema_param}.{myTable_param} set aggregates = NULL " + 
                          " where layer_name = {layerName_param} " + 
                          " and layer_id = {featureId_param} ");

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                           myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                           featureId_param   = sql.Literal( featureid ) , 
                           layerName_param   = sql.Literal( layerName ) 
                  ), [] 
           );

       cursor.close();
       connection.commit();

       print( "Aggregates has been removed from " + str( layerName ) );
       return { "success" : True };

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[deleteAggregates] : DB Connection Closed" );

 async def deleteMetadata( self , options ):
    connection = False;

    try: 
       entity           = options["entity"];
       layerName        = options["layername"];
       featureid        = options["featureid"] if ( "featureid" in options ) else "";
       queryReturn      = options["return"] if ( "return" in options ) else "*";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );
       cursor     = connection.cursor();
       
       if( featureid == "" ):
           myQuery    = ( "UPDATE {mySchema_param}.{myTable_param} set computed = NULL " + 
                          " where layer_name = {layerName_param} " );

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                           myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                           layerName_param   = sql.Literal( layerName ) 
                  ), [] 
           );
       else:
           myQuery    = ( "UPDATE {mySchema_param}.{myTable_param} set computed = NULL " + 
                          " where layer_name = {layerName_param} " + 
                          " and layer_id = {featureId_param} ");

           cursor.execute(
               sql.SQL( myQuery )
                  .format( 
                           mySchema_param    = sql.Identifier( self.cfg["database"]["schema"] ) , 
                           myTable_param     = sql.Identifier( self.cfg["database"]["tables"][ entity ] ) , 
                           featureId_param   = sql.Literal( featureid ) , 
                           layerName_param   = sql.Literal( layerName ) 
                  ), [] 
           );

       cursor.close();
       connection.commit();

       print( "Computed has been removed from " + str( layerName ) );
       return { "success" : True };

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[deleteMetadata] : DB Connection Closed" );

 async def deleteImage( self , options ):
    connection = False;
    entity     = "metadata";
    layerName  = options["layername"];
    featureid  = options["featureid"] if ( "featureid" in options ) else "";
    imageName  = options["imageName"] if ( "imageName" in options ) else "";
    connection = psycopg2.connect( user = self.cfg["database"]["connection"]["user"],
    password   = self.cfg["database"]["connection"]["password"],
    host       = self.cfg["database"]["connection"]["host"],
    port       = self.cfg["database"]["connection"]["port"],
    database   = self.cfg["database"]["connection"]["database"] );
    mySchema   = self.cfg["database"]["schema"];
    cursor     = connection.cursor();

    response = await self.getImage( {
        "featureid"   : featureid , 
        "layer_name"  : layerName 
    } );

    if( len( response ) <= 0 ) :
        return { "error" : "No available image" };

    myImagesObject = response;
    myNewObject    = list();

    try: 
       imageExists = False;
       for image in myImagesObject :
           print( image["filename"] + " | " + imageName );
           if( image["filename"] == imageName ):
               imageExists = True;
           else:
               myNewObject.append( image );

       if( imageExists is False ):
           return { "error" : "No image '"+imageName+"' available." };

       myQuery = ( "UPDATE "+str( mySchema )+".metadata " + 
                   " set image = '"+JSON.dumps( myNewObject )+"'::jsonb " + 
                   " where layer_id = "+str( featureid )+" " + 
                   " and layer_name = '"+str( layerName )+"' ;" );

       cursor.execute( myQuery );
       cursor.close();
       connection.commit();

       print( "Image has been removed from " + str( layerName ) );
       return { "success" : True };

    except( Exception, psycopg2.Error ) as error :
        print ( " << ERROR >> : " + str( error ) );
        return { "error" : error };

    except Exception as error:
        print ( " << ERROR >> : " + str( error ) );
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[deleteImage] : DB Connection Closed" );

 async def deleteImages( self , options ):
    connection = False;
    entity     = "metadata";
    layerName  = options["layername"];
    featureid  = options["featureid"] if ( "featureid" in options ) else "";
    connection = psycopg2.connect( user = self.cfg["database"]["connection"]["user"],
    password   = self.cfg["database"]["connection"]["password"],
    host       = self.cfg["database"]["connection"]["host"],
    port       = self.cfg["database"]["connection"]["port"],
    database   = self.cfg["database"]["connection"]["database"] );
    mySchema   = self.cfg["database"]["schema"];
    cursor     = connection.cursor();

    try: 

       myQuery = ( "UPDATE "+str( mySchema )+".metadata " + 
                   " set image = '[]'::jsonb " + 
                   " where layer_id = "+str( featureid )+" " + 
                   " and layer_name = '"+str( layerName )+"' ;" );

       cursor.execute( myQuery );
       cursor.close();
       connection.commit();

       print( "Images has been removed from " + str( layerName ) );
       return { "success" : True };

    except( Exception, psycopg2.Error ) as error :
        print ( " << ERROR >> : " + str( error ) );
        return { "error" : error };

    except Exception as error:
        print ( " << ERROR >> : " + str( error ) );
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[deleteImages] : DB Connection Closed" );

 async def addImage( self , options ):
    connection = False;

    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       layerName         = options["layerName"];
       layerId           = options["layerId"];
       filename          = options["filename"] if ( options["filename"] is not None ) else "";
       serviceEndpoint   = self.cfg["service"]["endpoint"];
       imagesServiceName = self.cfg["uploads"]["images"]["service_name"];
       mySchema          = self.cfg["database"]["schema"];
       myImagePath       = str(serviceEndpoint)+"/"+str(imagesServiceName)+"/"+str(filename);
       myImageObject     = dict();

     # Get current Image Object
       response = await self.getImage( {
         "featureid"   : layerId , 
         "layer_name"  : layerName 
       } );

       if( len( response ) > 0 ) :
           response.append( options["form"] );
           myQuery = ("UPDATE "+str( mySchema )+".metadata " + 
                      " set image = '"+JSON.dumps( response )+"'::jsonb " + 
                      " where layer_id = "+str( layerId )+" " + 
                      "and layer_name = '"+str( layerName )+"' ;");
       else:
           myQuery = ("UPDATE "+str( mySchema )+".metadata " + 
                      " set image = '["+JSON.dumps( options["form"] )+"]'::jsonb " + 
                      " where layer_id = "+str( layerId )+" " + 
                      "and layer_name = '"+str( layerName )+"' ;");

       cursor      = connection.cursor();

       print( "Inserting Image for Layer " + str( layerName ) );

       cursor.execute( myQuery );
       cursor.close();
       connection.commit();
       return { "res" : "success" };

    except( Exception, psycopg2.Error ) as error :
        print ( " << ERROR >> : " + str( error ) );
        return { "error" : error };

    except Exception as error:
        print ( " << ERROR >> : " + str( error ) );
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[addImage] : DB Connection Closed" );

 async def entryExist( self , options ):
    connection = False;
    try: 
       entity     = options["entity"];
       unique_id  = options["unique_id"];
       layerName  = options["layername"];
       featureid  = options["featureid"];
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor   = connection.cursor( cursor_factory=RealDictCursor );
       mySchema = self.cfg["database"]["schema"];
       myTable  = self.cfg["database"]["tables"][ entity ];
       myQuery  = ("select "+unique_id+" from {mySchema_param}.{myTable_param} " + 
                   "where layer_id = {featureid_param} " + 
                   "and layer_name = {layerName_param};");

       print( "Checking if "+entity+" exist for layer " + str( layerName ) );
       cursor.execute(
           sql.SQL( myQuery )
              .format( 
                       mySchema_param      = sql.Identifier( mySchema ) , 
                       myTable_param       = sql.Identifier( myTable ) , 
                       featureid_param     = sql.Literal( featureid ) , 
                       layerName_param     = sql.Literal( layerName ) 
              ), [] 
       );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if( cursor.rowcount <= 0 ):
           print( entity+" do not exist for layer " + str( layerName ) );
           return False;
       else:
           print( entity+" exist for layer " + str( layerName ) );
           return my_query_response[0][ unique_id ];

    except( Exception, psycopg2.Error ) as error :
        print ( "Error checking for "+entity+" on ", error );
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[entryExist] : DB Connection Closed" );

 async def addEntity( self , options ):
    connection       = False;
    entity           = options["entity"];
    layerName        = options["layername"];
    featureid        = options["featureid"];
    unique_id        = options["unique_id"];
    myJSON           = options["json"];
    isUpdatable      = options["isUpdatable"];
    allowMultiInsert = options["allowMultiInsert"];
    myValuesArray    = list();
    myTable          = self.cfg["database"]["schema"]+"."+self.cfg["database"]["tables"][ entity ];
    myFields         = options["fields"];
    hasData          = False;
    myInsertQuery    = ( 
                         "INSERT INTO "+myTable+" " + 
                         " ( \"" + ( "\" , \"".join(myFields) ) + "\" ) " + 
                         " VALUES " 
                       );
    myUpdateQuery    = ( "UPDATE "+myTable+" set " );
    hasEntry         = await self.entryExist({
                       "entity"    : entity , 
                       "unique_id" : unique_id , 
                       "layername" : layerName , 
                       "featureid" : featureid
                      });

    if( hasEntry is not False ) : 
        if( isUpdatable is True ) : 
            if( type( myJSON ).__name__ == "dict" ) : 
                print( "Updating " + entity );
                myValues = list();
                for fieldName in myFields :
                    if( fieldName == "layer_id" or fieldName == "layer_name" ):
                        pass;
                    else:
                        if( fieldName in myJSON ) : 
                            myValues.append( fieldName + " = '" + str( myJSON[ fieldName ] ) + "'" );
                if( len( myValues ) > 0 ) : 
                    myValuesArray.append( " , ".join( myValues ) );

                hasData        = True if ( len( myValuesArray ) > 0 ) else False;
                myExecuteQuery = ( 
                                   myUpdateQuery + 
                                   ",".join( myValuesArray ) + 
                                   " where layer_name = '"+layerName+"'" + 
                                   " and layer_id = '"+str(featureid)+"' "
                                  );
            else :
                print( "List Updates are not available" );
                return { "error" : "List Updates are not available" };
        else : 
            print( entity + " exist. Update Prohibited" );
            return { "error" : entity + " exist. Update Prohibited" };
    else:
        if( type( myJSON ).__name__ == "list" ):
            print( "Adding Multiple " + entity + " entries");

            if( allowMultiInsert == False ) : 
                print( "Multi insert not allowed" );
                return { "error" : "Multi insert not allowed" };
            else : 
                myValueGroups = list();
                for row in myJSON :
                    myValues = list();
                    for fieldName in myFields :
                        if( fieldName == "layer_id" ) :
                            myValues.append( str(featureid) );
                        elif( fieldName == "layer_name" ) :
                            myValues.append( str(layerName) );
                        else:
                            if( fieldName in row ) : 
                                myValues.append( str( row[ fieldName ] ) );
                            else:
                                myValues.append( "" );

                    if( len( myValues ) > 0 ) : 
                        myValuesString = "','".join( myValues );
                        myValueGroups.append( "('" + myValuesString + "')" );

                hasData = True if ( len( myValueGroups ) > 0 ) else False;
                myExecuteQuery = myInsertQuery + ",".join( myValueGroups );
        elif( type( myJSON ).__name__ == "dict" ):
            print( "Adding Single " + entity + " entry");

            myValues = list();
            for fieldName in myFields :
                if( fieldName == "layer_id" ) :
                    myValues.append( str(featureid) );
                elif( fieldName == "layer_name" ) :
                    myValues.append( str(layerName) );
                else:
                    if( fieldName in myJSON ) : 
                        myValues.append( str( myJSON[ fieldName ] ) );
                    else:
                        myValues.append( "" );

            if( len( myValues ) > 0 ) : 
                myValuesString = "','".join( myValues );
                myValuesArray.append( "('" + myValuesString + "')" );

            hasData = True if ( len( myValuesArray ) > 0 ) else False;
            myExecuteQuery = myInsertQuery + ",".join( myValuesArray );

    if( hasData == False ):
        return { "error" : "No Data to Insert or Update" };

    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor();

       cursor.execute( myExecuteQuery );

       cursor.close();
       connection.commit();
       return { "res" : entity + " Updated" };

    except( Exception, psycopg2.Error ) as error :
        print( error );
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[addJSON] : DB Connection Closed" );

 async def deleteTable( self , tableName ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );
       cursor     = connection.cursor();
       myQuery    = ( "drop table {mySchema_param}.{tableName_param};" );

       print( "Dropping Table " + str( tableName ) );
       cursor.execute(
           sql.SQL( myQuery )
              .format( 
                       mySchema_param  = sql.Identifier( self.cfg["database"]["schema"] ) , 
                       tableName_param = sql.Identifier( tableName ) 
              ), [] 
       );
       cursor.close();
       connection.commit();

    except( Exception, psycopg2.Error ) as error :
        print( "Error Dropping Table ", error );

    finally:
        if( connection ):
            connection.close();
            print( "[deleteTable] : DB Connection Closed" );

 async def deleteView( self , viewName ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );
       cursor  = connection.cursor();
       myQuery = ( "drop view {mySchema_param}.{viewName_param};" );

       print( "Dropping View " + str( viewName ) );
       cursor.execute(
           sql.SQL( myQuery )
              .format( 
                       mySchema_param = sql.Identifier( self.cfg["database"]["schema"] ) , 
                       viewName_param = sql.Identifier( viewName ) 
              ), [] 
       );
       cursor.close();
       connection.commit();

    except( Exception, psycopg2.Error ) as error :
        print( "Error Dropping View ", error );

    finally:
        if( connection ):
            connection.close();
            print( "[deleteView] : DB Connection Closed" );

 async def removePKConfiguration( self , tableName ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );
       cursor     = connection.cursor();
       print( "Removing Foreign Key configuration from " + str( self.cfg["database"]["tables"]["foreignkey"] ) );

       mySchema  = self.cfg["database"]["schema"];
       myPkTable = self.cfg["database"]["tables"]["foreignkey"];
       myQuery   = ( "delete from {mySchema_param}.{myPkTable_param} " + 
                     " where table_name = {tableName_param} " + 
                     " and table_schema = {mySchema_param_l};");
       cursor.execute(
           sql.SQL( myQuery )
              .format( 
                       mySchema_param     = sql.Identifier( mySchema ) , 
                       myPkTable_param    = sql.Identifier( myPkTable ) ,
                       tableName_param    = sql.Literal( tableName ) ,
                       mySchema_param_l   = sql.Literal( mySchema ) 
              ), [] 
       );
       cursor.close();
       connection.commit();

    except( Exception, psycopg2.Error ) as error :
        print( "Error Removing Foreign Key configuration ", error );

    finally:
        if( connection ):
            connection.close();
            print( "[removePKConfiguration] : DB Connection Closed" );

 async def getLayerData( self , layer_name , identifier = False ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );

       mySchema   = self.cfg["database"]["schema"];

       if( identifier == False ):
           myQuery = ( "select * from {mySchema_param}.{layer_name_param} limit 1000;" );
           qData   = sql.SQL( myQuery ).format( 
                         mySchema_param   = sql.Identifier( mySchema ),
                         layer_name_param = sql.Identifier( layer_name )
                     );
       else:
           myQuery = ( "select * from {mySchema_param}.{layer_name_param} where \"Identifier\" = {identifier_param};" );
           qData   = sql.SQL( myQuery ).format( 
                         mySchema_param   = sql.Identifier( mySchema ),
                         layer_name_param = sql.Identifier( layer_name ),
                         identifier_param = sql.Literal( identifier )
                     );
       cursor.execute( qData );

       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if ( cursor.rowcount > 0 ) :
           return my_query_response;
       else:
           return { "error" : "no data" };

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getLayerData] : DB Connection Closed" );

 async def getLayer( self , layer_name , identifier = False ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );

       mySchema   = self.cfg["database"]["schema"];

       if( identifier == False ):
           myQuery = ( "select * from {mySchema_param}.{layer_name_param};" );
           qData   = sql.SQL( myQuery ).format( 
                         mySchema_param   = sql.Identifier( mySchema ),
                         layer_name_param = sql.Identifier( layer_name )
                     );
       else:
           myQuery = ( "select * from {mySchema_param}.{layer_name_param} where \"Identifier\" = {identifier_param};" );
           qData   = sql.SQL( myQuery ).format(
                         mySchema_param   = sql.Identifier( mySchema ),
                         layer_name_param = sql.Identifier( layer_name ),
                         identifier_param = sql.Literal( identifier )
                     );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getLayerData] : DB Connection Closed" );

 async def getLayers( self ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );

       mySchema   = self.cfg["database"]["schema"];

       myQuery = ( "SELECT table_name FROM information_schema.tables " + 
                   "WHERE table_schema = {mySchema_param} " + 
                   "and table_type = 'VIEW' " + 
                   "and table_name not in ('geography_columns','geometry_columns');" );

       qData   = sql.SQL( myQuery ).format( 
                     mySchema_param   = sql.Literal( mySchema )
                 );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getLayerData] : DB Connection Closed" );

 async def hasParcel( self , featureID = False ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor();
       myQuery    = ( "select * from {mySchema_param}.metadata where \"Identifier\" = {featureID_param};" );
       qData      = sql.SQL( myQuery ).format(
                        mySchema_param   = sql.Identifier( self.cfg["database"]["schema"] ),
                        featureID_param  = sql.Literal( featureID )
                    );
       cursor.execute( qData );

       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if ( cursor.rowcount > 0 ) :
           return True;
       else:
           return False;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[hasEntity] : DB Connection Closed" );

 async def hasEntity( self , entity , featureID = False ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor();
       myQuery    = ( "select * from {mySchema_param}.{entity_param} where \"layer_id\" = {featureID_param};" );
       qData      = sql.SQL( myQuery ).format(
                        mySchema_param   = sql.Identifier( self.cfg["database"]["schema"] ),
                        entity_param     = sql.Identifier( entity ),
                        featureID_param  = sql.Literal( featureID )
                    );
       cursor.execute( qData );

       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if ( cursor.rowcount > 0 ) :
           return True;
       else:
           return False;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[hasEntity] : DB Connection Closed" );

 async def getEntity( self , layer_name , identifier = False ):
    connection = False;
    try: 
       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );
       mySchema   = self.cfg["database"]["schema"];
       myQuery    = ( "select * from {mySchema_param}.{layer_name_param} where \"Identifier\" = {identifier_param};" );
       qData      = sql.SQL( myQuery ).format(
                        mySchema_param   = sql.Identifier( mySchema ),
                        layer_name_param = sql.Identifier( layer_name ),
                        identifier_param = sql.Literal( identifier )
                    );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getLayerData] : DB Connection Closed" );

 async def getMetaData( self , options ):
    connection = False;
    try: 
       entity     = "metadata";
       layer_name = options["layer_name"] if ( "layer_name" in options  ) else "";
       featureid  = options["featureid"]  if ( "featureid" in options  ) else "";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );
       mySchema   = self.cfg["database"]["schema"];
       myQuery    = ( "select * from {mySchema_param}.{entity_param} " + 
                      "where layer_id = {featureid_param} " + 
                      "and layer_name = {layer_name_param};" );

       qData      = sql.SQL( myQuery ).format(
                        mySchema_param    = sql.Identifier( mySchema ),
                        entity_param      = sql.Identifier( entity ),
                        featureid_param   = sql.Literal( featureid ), 
                        layer_name_param  = sql.Literal( layer_name ) 
                    );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getMetaData] : DB Connection Closed" );

 async def getCalendarData( self , options ):
    connection = False;
    try: 
       layer_name = options["layer_name"] if ( "layer_name" in options  ) else "";
       featureid  = options["featureid"]  if ( "featureid" in options  ) else "";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );
       mySchema   = self.cfg["database"]["schema"];
       myQuery    = ( "select calendar from {mySchema_param}.metadata " + 
                      "where layer_id = {featureid_param} " + 
                      "and layer_name = {layer_name_param};" );

       qData      = sql.SQL( myQuery ).format(
                        mySchema_param    = sql.Identifier( mySchema ),
                        featureid_param   = sql.Literal( featureid ), 
                        layer_name_param  = sql.Literal( layer_name )
                    );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getCalendarData] : DB Connection Closed" );

 async def getAggregatesData( self , options ):
    connection = False;
    try: 
       layer_name = options["layer_name"] if ( "layer_name" in options  ) else "";
       featureid  = options["featureid"]  if ( "featureid" in options  ) else "";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );
       mySchema   = self.cfg["database"]["schema"];
       myQuery    = ( "select aggregates from {mySchema_param}.metadata " + 
                      "where layer_id = {featureid_param} " + 
                      "and layer_name = {layer_name_param};" );

       qData      = sql.SQL( myQuery ).format(
                        mySchema_param    = sql.Identifier( mySchema ),
                        featureid_param   = sql.Literal( featureid ), 
                        layer_name_param  = sql.Literal( layer_name )
                    );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       return my_query_response;

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getAggregatesData] : DB Connection Closed" );

 async def getImage( self , options ):
    connection = False;

    try: 
       entity     = "metadata";
       layer_name = options["layer_name"] if ( "layer_name" in options  ) else "";
       featureid  = options["featureid"]  if ( "featureid" in options  ) else "";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );
       mySchema   = self.cfg["database"]["schema"];
       myQuery    = ( "select image from {mySchema_param}.{entity_param} " + 
                      "where layer_id = {featureid_param} " + 
                      "and layer_name = {layer_name_param};" );

       qData      = sql.SQL( myQuery ).format(
                        mySchema_param    = sql.Identifier( mySchema ),
                        entity_param      = sql.Identifier( entity ),
                        featureid_param   = sql.Literal( featureid ), 
                        layer_name_param  = sql.Literal( layer_name ) 
                    );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if( len( my_query_response) > 0 ):
           if( len( my_query_response[0]["image"] ) <= 0 ):
               return [];
           else:
               return my_query_response[0]["image"];
       else:
           return [];

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getImage] : DB Connection Closed" );

 async def getFeaturesWithImage( self , options ):
    connection = False;

    try:
       entity     = "metadata";
       layer_name = options["layer_name"] if ( "layer_name" in options  ) else "";

       connection = psycopg2.connect( user     = self.cfg["database"]["connection"]["user"],
                                      password = self.cfg["database"]["connection"]["password"],
                                      host     = self.cfg["database"]["connection"]["host"],
                                      port     = self.cfg["database"]["connection"]["port"],
                                      database = self.cfg["database"]["connection"]["database"] );

       cursor     = connection.cursor( cursor_factory=RealDictCursor );
       mySchema   = self.cfg["database"]["schema"];
       myQuery    = ( "select layer_id , image from {mySchema_param}.{entity_param} " + 
                      "where layer_name = {layer_name_param} " + 
                      "and image::jsonb <> '[]'::jsonb;" );

       qData      = sql.SQL( myQuery ).format(
                        mySchema_param    = sql.Identifier( mySchema ),
                        entity_param      = sql.Identifier( entity ),
                        layer_name_param  = sql.Literal( layer_name ) 
                    );

       cursor.execute( qData );
       my_query_response = cursor.fetchall();
       cursor.close();
       connection.commit();

       if( len( my_query_response) > 0 ):
           return my_query_response;
       else:
           return [];

    except( Exception, psycopg2.Error ) as error :
        return { "error" : error };

    finally:
        if( connection ):
            connection.close();
            print( "[getImage] : DB Connection Closed" );
