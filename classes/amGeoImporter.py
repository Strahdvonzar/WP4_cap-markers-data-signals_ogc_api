import os;
import datetime;
import sys;
import aiohttp;
import asyncio;
import json as JSON;
from   classes.amTools  import amTool;

class amGeoImporter:
 def __init__(self):
  """Class for importing Files and Data 
  in the GeoServer."""
  self.amTool = amTool();
  self.amTool.log("Geo Importer Initiated");
  self.cfg    = self.amTool.load( "./cfg/" , "config" );
  self.myAuth = aiohttp.BasicAuth(
   self.cfg["geoserver"]["credentials"]["username"] , 
   self.cfg["geoserver"]["credentials"]["password"] 
  );
  self.cfg
  self.TaskID = 0;

 async def layerExists( self , work_space , layer_name ):
     async with aiohttp.ClientSession() as session:

          endPoint = ( self.cfg["geoserver"]["base"] + 
                       "/rest/workspaces/" + work_space + 
                       "/layers/" + layer_name );

          async with session.get( endPoint , auth=self.myAuth , headers={'content-type': 'application/json' , 'accept' : 'application/json' } ) as response:
               myText = await response.text();
               self.amTool.log_curl( "get" , "" , endPoint , False , True );

               try:
                   myJSON = JSON.loads( myText );
               except ValueError as error :
                   return False;
               return True;

 async def addLayer( self , work_space , store_name , layer_name , bbox , layer_srs="EPSG:4326" ):
     async with aiohttp.ClientSession() as session:
          postData = ( "<featureType>" + 
                         "<name>" + layer_name + "</name>" + 
                         "<nativeCRS>" + layer_srs + "</nativeCRS>" + 
                         "<srs>" + layer_srs + "</srs>" + 
                         # "<nativeBoundingBox>" + 
                           # "<minx>" + str( bbox["minx"] ) + "</minx>" + 
                           # "<maxx>" + str( bbox["maxx"] ) + "</maxx>" + 
                           # "<miny>" + str( bbox["miny"] ) + "</miny>" + 
                           # "<maxy>" + str( bbox["maxy"] ) + "</maxy>" + 
                           # "<crs>" + layer_srs + "</crs>" + 
                         # "</nativeBoundingBox>" + 
                        "</featureType>");

          endPoint = ( self.cfg["geoserver"]["base"] + 
                       "/rest/workspaces/" + work_space + 
                       "/datastores/" + store_name + "/featuretypes" );

          async with session.post( endPoint , auth=self.myAuth , headers={'content-type': 'text/xml'} , data=postData ) as response:
               myText = await response.text();

               self.amTool.log_curl( "post" , postData , endPoint , False , True );
               return myText;

 async def deleteLayer( self , work_space , layer_name ):
     async with aiohttp.ClientSession() as session:
          myURL = self.cfg["geoserver"]["base"] + "/rest/layers/"+work_space+":"+layer_name+".xml";
          async with session.delete( myURL , auth=self.myAuth ) as response:
               myText = await response.text();
               self.amTool.log( myURL );
               return myText;

 async def deleteLayerSimple( self , work_space , layer_name ):
     async with aiohttp.ClientSession() as session:
          myURL = self.cfg["geoserver"]["base"] + "/rest/workspaces/"+work_space+"/datastores/"+self.cfg["geoserver"]["storename"]+"/featuretypes/"+layer_name+".xml";
          async with session.delete( myURL , auth=self.myAuth ) as response:
               myText = await response.text();
               self.amTool.log( myURL );
               return myText;

 async def deleteLayerGWC( self , work_space , layer_name ):
     async with aiohttp.ClientSession() as session:
          myURL = self.cfg["geoserver"]["base"] + "/gwc/rest/layers/"+work_space+":"+layer_name+".xml";
          async with session.delete( myURL , auth=self.myAuth ) as response:
               myText = await response.text();
               self.amTool.log( myURL );
               return myText;

 async def createTask( self , options ):
     postData = options;
     async with aiohttp.ClientSession() as session:
          endPoint = self.cfg["geoserver"]["endpoint"];
          async with session.post( endPoint , auth=self.myAuth , json=postData ) as response:
               myText      = await response.text();
               myJSON      = JSON.loads(myText);
               self.TaskID = myJSON["import"]["id"];

               self.amTool.log_curl( "post" , JSON.dumps(postData).replace("\"" , "\\\"") , endPoint );
               self.amTool.log( "Task with ID ["+ str(self.TaskID) +"] has been created" );
               return myJSON;

 async def uploadFile( self , filePath , fileName , fileContentType ):
     
     data = aiohttp.FormData();
     data.add_field('file',
                    open(filePath, 'rb'),
                    filename=fileName,
                    content_type=fileContentType);
     async with aiohttp.ClientSession() as session:
          endPoint = self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID)+"/tasks";
          async with session.post( endPoint , auth=self.myAuth , data=data ) as response:
               myText = await response.text();
               self.amTool.log_curl( "post" , {} , endPoint , { "name" : fileName , "path" : filePath } );
               return JSON.loads( myText );

 async def addProjection( self , projection):
     postData = {"layer":{"srs": projection }};
     async with aiohttp.ClientSession() as session:
          endPoint = self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID)+"/tasks/0/layer/";
          async with session.put( endPoint , auth=self.myAuth , json=postData ) as response:
               myText = await response.text();
               self.amTool.log_curl( "put" , JSON.dumps(postData).replace("\"" , "\\\"") , endPoint );
               return JSON.loads( myText );

 async def addName( self , layerName ):
     postData = {"layer":{"name": layerName }};
     async with aiohttp.ClientSession() as session:
          endPoint = self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID)+"/tasks/0/layer/";
          async with session.put( endPoint , auth=self.myAuth , json=postData ) as response:
               myText = await response.text();
               self.amTool.log_curl( "put" , JSON.dumps(postData).replace("\"" , "\\\"") , endPoint );
               return JSON.loads( myText );

 async def updateTest( self ):
     myProperty = "name";
     myValue    = "kostas";
     myLayer    = "NIVA:data";
     myID       = "data.1";
     postData = f'<wfs:Transaction service="WFS" version="1.0.0" xmlns:topp="http://www.openplans.org/topp" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wfs="http://www.opengis.net/wfs"> <wfs:Update typeName="{myLayer}"> <wfs:Property><wfs:Name>{myProperty}</wfs:Name> <wfs:Value>{myValue}</wfs:Value></wfs:Property>   <ogc:Filter><ogc:FeatureId fid="{myID}"/></ogc:Filter></wfs:Update> </wfs:Transaction>';

     async with aiohttp.ClientSession() as session:
          async with session.post( self.cfg["geoserver"]["base"] + "/wfs" , auth=self.myAuth , data=postData ) as response:
               myText = await response.text();
               return JSON.loads( myText );

 async def insertTest( self ):
     postData = '<wfs:Transaction service="WFS" version="1.0.0" xmlns:topp="http://www.openplans.org/topp" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wfs="http://www.opengis.net/wfs"><wfs:Update typeName="NIVA:simple0"><wfs:Property><wfs:Name>address</wfs:Name><wfs:Value>tascasc</wfs:Value></wfs:Property><ogc:Filter><ogc:FeatureId fid="simple0.1"/></ogc:Filter></wfs:Update></wfs:Transaction>';

     async with aiohttp.ClientSession() as session:
          async with session.post( self.cfg["geoserver"]["base"] + "/wfs" , auth=self.myAuth , data=postData ) as response:
               myText = await response.text();
               return JSON.loads( myText );

 async def changeMode( self , mode ):
     # Modes : APPEND , CREATE , REPLACE
     postData = {"task":{"updateMode":mode}};
     async with aiohttp.ClientSession() as session:
          endPoint = self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID)+"/tasks/0";
          async with session.put( endPoint , auth=self.myAuth , json=postData ) as response:
               myText = await response.text();
               self.amTool.log_curl( "put" , JSON.dumps(postData).replace("\"" , "\\\"") , endPoint );
               return JSON.loads( myText );

 async def mapAttributes( self , mapping ):
     postData = mapping;
     async with aiohttp.ClientSession() as session:
          async with session.post( self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID)+"/tasks/0/transforms" , auth=self.myAuth , json=postData ) as response:
               myText = await response.text();
               return JSON.loads( myText );

 async def getResult( self ):
     async with aiohttp.ClientSession() as session:
          endPoint = self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID)+"/tasks/0";
          async with session.get( endPoint , auth=self.myAuth ) as response:
               myText = await response.text();
               return JSON.loads( myText );

 async def executeTask( self ):
     postData = {};
     async with aiohttp.ClientSession() as session:
          endPoint = self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID);
          async with session.post( self.cfg["geoserver"]["endpoint"]+"/"+str(self.TaskID) , auth=self.myAuth , json=postData) as response:
               myText = await response.text();
               self.amTool.log_curl( "post" , JSON.dumps(postData).replace("\"" , "\\\"") , endPoint );
               return myText;

 async def truncateLayer( self , layer_name ):
     async with aiohttp.ClientSession() as session:
          postData = ( "<truncateLayer>" + 
                         "<layerName>" + layer_name + "</layerName>" + 
                        "</truncateLayer>");
          endPoint = self.cfg["geoserver"]["base"] + "/gwc/rest/masstruncate";
          async with session.post( endPoint , auth=self.myAuth , headers={'content-type': 'text/xml'} , data=postData ) as response:
               myText = await response.text();
               self.amTool.log_curl( "post" , postData , endPoint , False , True );
               return myText;

 async def getLayers( self ):
     async with aiohttp.ClientSession() as session:

          endPoint = ( self.cfg["geoserver"]["base"] + 
                       "/rest/workspaces/" + self.cfg["geoserver"]["workspace"] + 
                       "/layers" );

          async with session.get( endPoint , auth=self.myAuth , headers={'content-type': 'application/json' , 'accept' : 'application/json' } ) as response:
               myText = await response.text();
               self.amTool.log_curl( "get" , "" , endPoint , False , True );

               try:
                   myJSON = JSON.loads( myText );
                   return myJSON;
               except ValueError as error :
                   return False;