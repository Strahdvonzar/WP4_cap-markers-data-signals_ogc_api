import os;
import datetime;
import sys;
import json as JSON;
import hashlib;
from time import time;

class amTool:
 def __init__( self ):
  """Class for Misc Tools"""
  self.log( "Tools Initiated" );
  self.cfg = self.load( "./cfg/" , "config" );

 def log( self , myString ):
     myCurrentTime = datetime.datetime.now();
     print( "[" + datetime.datetime.strftime( myCurrentTime , "%Y-%m-%d %H:%M:%S" ) + "] : " + str( myString ) );

 def load( self , filePath , fileName ):
     """
      Loads Configuration from Disk
     """
     myCFG = dict();
     try:
         fileName = filePath + fileName + ".json";
         if( os.path.exists( fileName ) == False ):
            self.log("[ amModel Error ] : File [" + fileName + "] Not Found");
            return False;

         f = open( fileName, "r" );
         if f.mode == "r":
             myContents = f.read();
             try:
                 myCFG = dict( JSON.loads( myContents ) );
                 self.log( "File ["+fileName+"] loaded." );
             except:
                 self.log( "File ["+fileName+"] could not be loaded. Reason : ["+ str(sys.exc_info()[1]) +"]" );
         f.close();
         return myCFG;
     except:
         self.log( "Unexpected error upon loading Storage ["+fileName+"]:" + str( sys.exc_info()[1] ) );
         return False;

 def logFile( self , myString) : 
     myCurrentTime = datetime.datetime.now();
     myPrefix = "[" + datetime.datetime.strftime( myCurrentTime , "%Y-%m-%d %H:%M:%S" ) + "] : ";

     fd = open( os.path.abspath( self.cfg["logs"]["path"]+"log.log" ) , "a+");
     fd.write( myPrefix + str( myString ) + "\n");
     fd.close();

 def log_curl( self , method , postData , endpoint , file=False , layer=False ):
     myMethod      = "";
     myContentType = "Content-type: application/json";

     if( method     == "post" ):   myMethod = "-XPOST";
     elif( method   == "get" ):    myMethod = "-XGET";
     elif( method   == "delete" ): myMethod = "-XDELETE";
     elif( method   == "put" ):    myMethod = "-XPUT";
     else: myMethod =  "XGET";

     if( file == False ):
         myLogString = ('curl -u username:password ' + 
                        myMethod + 
                        ' -H \"'+myContentType+'\" ' + 
                        ' -d \"'+postData+'\" ' + 
                        ' \"'+endpoint+'\" ');
     else:
         myLogString = 'curl -u username:password -F name='+file["name"]+' -F filedata=@'+file["path"]+' "'+endpoint+'"';

     if( layer != False ):
         myLogString = ('curl -v -u username:password ' + 
                        myMethod + 
                        ' -H \"Content-type: text/xml\" ' + 
                        ' -d \"'+postData+'\" ' + 
                        ' \"'+endpoint+'\" ');         

     fd = open( os.path.abspath( self.cfg["logs"]["path"]+"log.log" ) , "a+");
     fd.write( myLogString + "\n");
     print( "CURL >>>" + str( myLogString ) );
     fd.close();

 def getUID( self , hash ):
     currentDate     = datetime.datetime.now();
     currentTS       = currentDate.timestamp();
     myStringHash    = str( currentTS ) + str( hash );
     
     return myStringHash;

 def isValidJSON( self , json ):
     try:
         myJSON = JSON.loads( json );
     except ValueError as error :
         return False;
     return True;