# Introduction

This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

<h3 align="center">CAP Markers & Data Signals Sharing Component</h3>

<p align="center">
  This common component aims to act as a single point of access,
  capable to provide heterogeneous information items derived by multiple sources
  through a standardised OGC compatible API. Information sources examples include
  EO classification engines outcomes (e.g. crop type), geotagged photos and farm management information systems.
</p>

<p align="center">
  Data Import is based on REST API webservice.
  Data export on OGC API (Geoserver Imlementation)
</p>

<p align="center">
  The following Technolgies / APIs / Libraries are utilised:
  <br>
  Python 3 : <a href="https://docs.python.org/3/"><strong>Explore Python 3.6+ docs »</strong></a>
  <br>
  FastAPI : <a href="https://fastapi.tiangolo.com/"><strong>FastAPI »</strong></a>
  <br>
  GeoServer : <a href="https://docs.geoserver.org/"><strong>Explore GeoServer docs »</strong></a>
</p>


## Table of contents

- [Quick start](#quick-start)
- [Endpoint Documentation](#endpoint-documentation)
- [Web Service Endpoints](#web-service-endpoints)
- [Testing it out Instructions](#testing-it-out-instructions)


## Quick start

### This API requires the Importer Extension of GeoServer
- Go to "http://geoserver.org/release/stable/"
- Go to Miscellaneous -> Importer -> Core 
- Download the extension
- Extract it somewhere
- Stop the GEOSERVER
- Copy all the contents ( *.jar files ) in your extension directory which can be found at "/geoserver-x.xx.x/webapps/geoserver/WEB-INF/lib"
- Start the GEOSERVER

### Python and Libraries

- Install [Python 3.6+](https://www.python.org/downloads/)
- Install an ASGI Server like Uvicorn : 

```console
$ pip install uvicorn
```

- Install the following Python libraries : 

```console
$ pip install aiohttp
$ pip install python-multipart
$ pip install aiofiles
$ pip install psycopg2
$ pip install fastapi
```

### PostGIS and Topology on PostgreSQL

- Enable PostGIS and Topology Extension in your Database. ( If you already have a PostgreSQL with PostGIS and Topology enabled skip this step )
```console
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
```

### Configuration Files

- Set the correct Database Info in "/cfg/config.json"

- Execute "setup_db.py" found in the root of the project.

### Start API Server

- Start Uvicorn

```console
$ uvicorn main:app --reload
```

## Endpoint Documentation 
You can set the Documentation path by changing the "docs_url" in "config.json" which can be found inside "/cfg" : 

```console
localhost:8000/docs
```

## Web Service Endpoints
All Requests must have the Basic Authentication properties "username:password"

| GET | Get List of Layers |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers" |
| Method | GET |
| Example | curl -u demo:123456 -X GET "http://localhost:8000/layers" |


---


| POST | Create Layer |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers" |
| Method | POST |
| Example | curl -u demo:123456 -X POST -F filedata=@shapefile_001.zip "http://localhost:8000/layers/shapefile_001" |

| Field | Type |
| ------ | ------ |
| filedata |  [file] |


---


| GET | Get Layer Data |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}" |
| Method | GET |
| Example | curl -u demo:123456 -X GET "http://localhost:8000/layers/shapefile_001" |


---


| DELETE | Delete Layer |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}" |
| Method | DELETE |
| Example | curl -u demo:123456 -X DELETE "http://localhost:8000/layers/shapefile_001" |


---


| GET | Get specific Parcel Data |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}" |
| Method | GET |
| Example | curl -u demo:123456 -X GET "http://localhost:8000/layers/shapefile_001/parcels/1111111111" |


---


| PUT | Add or Update Parcel MetaData |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}/metadata" |
| Method | PUT |
| BODY | JSON |
| Example | curl -u demo:123456 -X PUT -d "{\"calculationdate\": \"17-02-2021 10:00:10\",\"rulename\": \"basicpaymentscheme\",\"colorcode\": \"Green\",\"parcelcode\": 43540387,\"cultivationdeclaredcode\": 115,\"cultivationdeclaredname\":\"rapeseed\" , \"cultivation1stpredictioncode\": 115,\"probability1stprediction\": 0.78,\"cultivation2ndpredictioncode\":330,\"probability2stprediction\":0.20}" "http://localhost:8000/layers/shapefile_001/parcels/1111111111/metadata" |


---


| DELETE | Delete Parcel MetaData |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}/metadata" |
| Method | DELETE |
| Example | curl -u demo:123456 -X DELETE "http://localhost:8000/layers/shapefile_001/parcels/1111111111/metadata" |


---


| PUT | Add or Update Parcel Calendar |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}/calendar" |
| Method | PUT |
| BODY | JSON |
| Example | curl -X PUT -u demo:123456 -d "{\"protocol\" : {\"name\" : \"ecrop\",\"standard\": \"UN/CEFACT\"},\"calendar\" : [{\"TypeCode\": { \"Code\": \"1\", \"Name\": \"Pesticide\" },\"ActualStartDateTime\": \"2020-04-20T10:18:16Z\",\"ActualEndDateTime\": \"2020-04-21T10:18:16Z\",\"AppliedSpecifiedAgricultureApplication\": [{\"SpecifiedProductBatch\": [{\"ProductName\" : \"No Name\",\"WeightMeasure\": { \"Value\": 460, \"unitCode\": \"gr\" },\"UnitQuantity\" : 2 , \"SpecifiedAgricultureInputProduct\": [{\"Name\": \"Flutolanil\" }]}]}]},{\"TypeCode\": { \"Code\": \"1\", \"Name\": \"Fertilizer\" },\"ActualStartDateTime\": \"2020-04-20T10:18:16Z\",\"ActualEndDateTime\": \"2020-04-21T10:18:16Z\",\"AppliedSpecifiedAgricultureApplication\": [{\"SpecifiedProductBatch\": [{\"ProductName\" : \"Ultrasol\",\"WeightMeasure\": { \"Value\": 25, \"unitCode\": \"kg\" },\"UnitQuantity\" : 20 , \"SpecifiedAgricultureInputProduct\": [{\"Name\": \"Potassium Nitrate\" }]}]}]}]}" "http://localhost:8000/layers/shapefile_001/parcels/1111111111/calendar" |


---


| DELETE | Delete Parcel Calendar |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}/metadata" |
| Method | DELETE |
| Example | curl -X DELETE -u demo:123456 "http://localhost:8000/layers/shapefile_001/parcels/1111111111/calendar" |


---


| PUT | Upload Image |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}/images" |
| Method | PUT |
| Example | curl -u demo:123456 -X PUT -F location_coordinates_x=25.269230615119 -F location_coordinates_y=40.962504122927 -F gyroscopic_x=0.7733285427093506 -F gyroscopic_y=0.028375301510095596 -F gyroscopic_z=-0.3877149224281311 -F description="this is an image from my parcel" -F Manufacturer="Samsung" -F Platform="Android" -F Version=10 -F Date="17/01/2021 10:00:10" -F filedata=@my_image.png "http://localhost:8000/layers/shapefile_001/parcels/1111111111/images" |

| Field | Type |
| ------ | ------ |
| location_coordinates_x | [float]  |
| location_coordinates_y | [float]  |
| gyroscopic_x           | [float]  |
| gyroscopic_y           | [float]  |
| gyroscopic_z           | [float]  |
| description            | [string] |
| Manufacturer           | [string] |
| Platform               | [string] |
| Version                | [string] |
| Date                   | [string] |
| filedata               | [file]   |
| filedata               | [file]   |


---


| DELETE | Delete Parcel Image/s |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}/images" |
| Method | DELETE |
| Example | curl -X DELETE -u demo:123456 "http://localhost:8000/layers/shapefile_001/parcels/1111111111/images" |


---


| DELETE | Delete Parcel specific Image  |
| ------ | ------ |
| End Point | "WEB_SERVICE_URL/layers/{**layer_name**}/parcels/{**parcel_id**}/images/{image_id}" |
| Method | DELETE |
| Example | curl -X DELETE -u demo:123456 "http://localhost:8000/layers/shapefile_001/parcels/1111111111/images/1" |


# Testing it out Instructions

This section expects you to have : 
- A GeoServer installed and running 
- A PostgreSQL installed , running with the PostGIS plugin.
- A ASGI Server installed and Running with your Python Code
Note : Your GeoServer should have the Importer plugin installed.

## Geoserver : Create new Workspace
- Go To Data -> Workspaces
- Select "Add new workspace"
- Fill in the name and the URI and press save

## Geoserver : Create new Store
- Go To Data -> Stores 
- Select "Add new Store"
- In the "Vector Data Sources" select "PostGIS" Database
- Select your previously created Workspace in the drop down menu
- Provide a DataSource Name 
- Provide the Connection String infomration
- Skip to "Primary key metadata table" and fill the name of the table you created for the FIDs creation (Example : gt_pk_metadata)

## Python Code : Setup your WebService Configuration File
Navigate to your WebService source code and edit "/cfg/config.json" with the appropriate info.
- geoserver -> endpoint : Is the base url of your Geoserver followed by "/rest/imports". For Example "http://localhost:8080/geoserver/rest/imports"
- geoserver -> base : Is the base url of your Geoserver. For Example "http://localhost:8080/geoserver"
- geoserver -> credentials : Your GeoServer Admin Username and Password
- service -> endpoint : Is the root url of your (FastApi) Web Service. For Example "http://localhost:8000"
- logs -> path : Is the Path to where the Log files will be created.
- database -> connection : Your Database connection String details.
- database -> schema : The Schema that will be used for Layer Data.
- database -> tables : The name of the Tables you want to be created in the Schema and will keep the corresponding information.
- uploads -> tmp : Is the path to the Temporary folder where files will be uploaded.
- uploads -> images -> : Is the path to the folder where images will be uploaded.
- uploads -> service_name -> : Provide the name of the folder where images will be uploaded.

## Try it out
You can now try it out using the cUrls in [Web Service Endpoints](#web-service-endpoints)
Always start with teh Create Layer action. You can not upload anything when there is no Layer to accept them.

